/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { OverScrollDecor } from '@ohos/overscroll-decor';
import SideBar from "./SideBar";

@Entry
@ComponentV2
struct MiscViewsDemo {
  @Local timer: string = ''
  @Param private textView: OverScrollDecor.Model = new OverScrollDecor.Model()
  @Param private imageView: OverScrollDecor.Model = new OverScrollDecor.Model()
  @Local private chronometer: OverScrollDecor.Model = new OverScrollDecor.Model()
  @Local private isVisibility: Visibility = Visibility.None
  @Local private backOffset: number = 0

  private chronometerView() {
    let min = 0
    let sec = 0
    let that = this
    setInterval(() => {
      sec++
      if (sec == 60) {
        sec = 0
        min++
        if (min == 60) {
          min = 0
        }
      }
      let minutes = min < 10 ? '0' + min : min
      let seconds = sec < 10 ? '0' + sec : sec
      that.timer = minutes + ' : ' + seconds
    }, 1000);
  }

  aboutToAppear() {
    this.chronometerView()

    this.textView
      .setUpOverScroll(true)
      .setOrientation(OverScrollDecor.ORIENTATION.HORIZONTAL)
      .setOverScrollBounceEffect(true)
      .setScrollBar(false)
      .setWidth("100%")
      .setHeight("28%")

    this.imageView
      .setUpOverScroll(true)
      .setOrientation(OverScrollDecor.ORIENTATION.VERTICAL)
      .setOverScrollBounceEffect(true)
      .setScrollBar(false)
      .setWidth("100%")
      .setHeight("36%")

    this.chronometer
      .setUpOverScroll(true)
      .setOrientation(OverScrollDecor.ORIENTATION.HORIZONTAL)
      .setOverScrollBounceEffect(true)
      .setScrollBar(false)
      .setWidth("100%")
      .setHeight("28%")
  }

  @Builder textViewScroll() {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center}) {
      Text("Practically any clickable widget can be over-scrolled.\nTry it!")
        .textAlign(TextAlign.Center)
        .fontSize(30).width("75%")
        .fontColor("#727171")
    }.width("105%")
  }

  @Builder imageViewScroll() {
    Flex({ justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Image($r('app.media.image')).objectFit(ImageFit.Contain)
        .width("33%").height("50%")
    }.width("100%").height("105%")
  }

  @Builder chronometerViewScroll() {
    Flex({ direction: FlexDirection.Column, justifyContent: FlexAlign.Center, alignItems: ItemAlign.Center }) {
      Row({ space: 5 }) {
        Text('' + this.timer).fontSize(50)
      }
    }.width("105%")
  }

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Row() {
          Image($r('app.media.ic_more')).objectFit(ImageFit.Contain)
            .width("10%").height("65%").margin({ left: 16, right: 20 })
            .onClick((event?: ClickEvent) => {
              let that = this
              this.backOffset = -1
              let intervalID = setInterval(() => {
                that.backOffset += 0.1
              }, 40);
              let timeoutID = setTimeout(() => {
                clearInterval(intervalID)
                that.backOffset = 0
              }, 400);
              this.isVisibility = Visibility.Visible
            })
          Text("Misc. Over-Scroll Demo")
            .fontSize(38)
            .maxLines(1)
            .fontColor(Color.White)
            .textOverflow({ overflow: TextOverflow.Ellipsis })
            .width("80%")
        }.width("100%").height("8%").backgroundColor("#ffffbb33")

        Text(this.timer).visibility(Visibility.None)
        OverScrollDecor({ model: this.textView!!, child: () => { this.textViewScroll() } })
        OverScrollDecor({ model: this.imageView!!, child: () => { this.imageViewScroll() } })
        OverScrollDecor({ model: this.chronometer!!, child: () => { this.chronometerViewScroll() } })
      }
      Stack({ alignContent: Alignment.TopStart }) {
        Flex() {
        }.width("100%").height("100%")
        .backgroundColor(Color.Black).opacity(0.5 + this.backOffset * 0.3)
        .onClick((event?: ClickEvent) => {
          let that = this
          let intervalID = setInterval(() => {
            that.backOffset -= 0.1
          }, 40);
          let timeoutID = setTimeout(() => {
            clearInterval(intervalID)
            that.backOffset = 0
            that.isVisibility = Visibility.None
          }, 400);
        })
        Flex() {
          SideBar()
        }.offset({ x: this.backOffset * 500, y: 0 })
      }.width("100%").height("100%").visibility(this.isVisibility)
    }
  }
}
