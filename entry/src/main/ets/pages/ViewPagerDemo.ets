/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { OverScrollDecor, ViewPagerOverScroll } from '@ohos/overscroll-decor';
import SideBar from "./SideBar";

@Entry
@ComponentV2
struct ViewPagerDemo {
  private color: Array<string> =
    ["#ffaa66cc", "#ff0099cc", "#ff33b5e5", "#00e0e0", "#ff669900", "#ff99cc00",
    "#f0e000", "#ffffbb33", "#ffff8800", "#ffff4444", "#ffcc0000"]
  private colorName: Array<string> =
    ["PURPLE", "BLUE", "LIGHT BLUE", "CYAN", "GREEN", "LIGHT GREEN",
    "YELLOW", "LIGHT ORANGE", "ORANGE", "LIGHT RED", "RED"]
  private controller: TabsController = new TabsController()
  private itemIndex: number = 0
  @Param private model: OverScrollDecor.Model = new OverScrollDecor.Model()
  @Local private isVisibility: Visibility = Visibility.None
  @Local private backOffset: number = 0

  aboutToAppear() {
    this.model.setHeight("92%")
  }

  @Builder SquareText(index: number) {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Text(this.colorName[index]).fontSize(28).textAlign(TextAlign.Center)
    }.height("100%").width("100%")
    .backgroundColor(this.color[index])
  }

  @Builder SpecificChild() {
    Tabs({ index: this.itemIndex, controller: this.controller }) {
      TabContent() {
        this.SquareText(0)
      }
      TabContent() {
        this.SquareText(1)
      }
      TabContent() {
        this.SquareText(2)
      }
      TabContent() {
        this.SquareText(3)
      }
      TabContent() {
        this.SquareText(4)
      }
      TabContent() {
        this.SquareText(5)
      }
      TabContent() {
        this.SquareText(6)
      }
      TabContent() {
        this.SquareText(7)
      }
      TabContent() {
        this.SquareText(8)
      }
      TabContent() {
        this.SquareText(9)
      }
      TabContent() {
        this.SquareText(10)
      }
    }
    .height("100%")
    .width("100%")
    .barWidth(0)
    .barHeight(0)
    .padding(16)
  }

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
        Row() {
          Image($r('app.media.ic_more')).objectFit(ImageFit.Contain)
            .width("10%").height("65%").margin({ left: 16, right: 20 })
            .onClick((event?: ClickEvent) => {
              let that = this
              this.backOffset = -1
              let intervalID = setInterval(() => {
                that.backOffset += 0.1
              }, 40);
              let timeoutID = setTimeout(() => {
                clearInterval(intervalID)
                that.backOffset = 0
              }, 400);
              this.isVisibility = Visibility.Visible
            })
          Text("ViewPager Over-Scroll Demo")
            .fontSize(38)
            .maxLines(1)
            .fontColor(Color.White)
            .textOverflow({ overflow: TextOverflow.Ellipsis })
            .width("80%")
        }.width("100%").height("8%").backgroundColor("#ffffbb33")

        ViewPagerOverScroll({ model: this.model!!, child: () => { this.SpecificChild() } })
      }

      Stack({ alignContent: Alignment.TopStart }) {
        Flex() {
        }.width("100%").height("100%")
        .backgroundColor(Color.Black).opacity(0.5 + this.backOffset * 0.3)
        .onClick((event?: ClickEvent) => {
          let that = this
          let intervalID = setInterval(() => {
              that.backOffset -= 0.1
            }, 40);
          let timeoutID = setTimeout(() => {
             clearInterval(intervalID)
             that.backOffset = 0
             that.isVisibility = Visibility.None
            }, 400);
        })
        Flex() {
          SideBar()
        }.offset({ x: this.backOffset * 500, y: 0 })
      }.width("100%").height("100%").visibility(this.isVisibility)
    }
  }
}
